# Chocolate

## Crawl Spider

```
scrapy crawl choco -o ./data/test.json

```


## Project Description 
Need to scrap all the products with following details.
- Product Title
- Product Price
- Product Weight
- Product Description
- Product Url
- Product Image



##  Technical Description 
Project contain two type of urls 
- https://www.chocolate.co.uk/products/box-of-the-month-subscription-1
- https://www.chocolate.co.uk/collections/luxury-chocolate-bars

scrap prodcut using 

- CrawlSpider
- Rule
- Link LinkExtractor


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ecommerace/chocolate.git
git branch -M main
git push -uf origin main
```

 