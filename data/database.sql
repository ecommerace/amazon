CREATE TABLE [dbo].[AmazonReview1](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProfileName] [varchar](500) NULL,
	[ReviewTitle] [varchar](500) NULL,
	[ReviewDate] [varchar](500) NULL,
	[ReviewText] [varchar](max) NULL,
	[ReviewStar] [varchar](50) NULL,
	[ProductCode] [varchar](250) NULL,
	[PorductTitle] [varchar](500) NULL,
	[ProductURL] [varchar](500) NULL,
	[CreateDate] [datetime] NULL default getdate()
)