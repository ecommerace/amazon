import pymssql
from itemadapter import ItemAdapter
import pypyodbc as odbc

class AmazonReviewPipeline(object):
    def __init__(self, sqlServer,db):

        DRIVER = 'SQL Server'
        SERVER_NAME = sqlServer
        DATABASE_NAME = db
        conn_string = f"""
            Driver={{{DRIVER}}};
            Server={SERVER_NAME};
            Database={DATABASE_NAME};
            Trust_Connection=yes;
        """
        self.sql = """
            INSERT INTO [dbo].[AmazonReview]
                       ([ProductUrl]
                       ,[PorductTitle]
                       ,[ProductCode]
                       ,[ProfileName]
                       ,[ReviewTitle]
                       ,[ReviewDate]
                       ,[ReviewText]
                       ,[ReviewStar])
                 VALUES (?,?,?,?,?,?,?,?)
           """
        self.conn = odbc.connect(conn_string)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        try:
            adapter = ItemAdapter(item)
            values = (
                adapter.get('ProductURL'),
                adapter.get('PorductTitle'),
                adapter.get('ProductCode'),
                adapter.get('ProfileName'),
                adapter.get('ReviewTitle'),
                adapter.get('ReviewDate'),
                adapter.get('ReviewText'),
                adapter.get('ReviewStar')
            )
            self.cursor.execute(self.sql, values)
            self.conn.commit()
        except (pymssql.Error):
            print("error")
        return item

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            sqlServer=crawler.settings.get('SERVER_NAME'),
            db=crawler.settings.get('DATABASE_NAME'),
        )
