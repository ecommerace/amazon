# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html


from scrapy.loader.processors import TakeFirst, MapCompose, Join
from w3lib.html import remove_tags, strip_html5_whitespace
import scrapy

def get_review(start_text):
    starts = start_text.split()[0]
    return starts

def get_date(date_text):
    date = date_text.split('on')[-1]
    return date
def get_SKU(URL):
    #/AmazonBasics-Performance-Alkaline-Batteries-Count/dp/B00MNV8E0C/ref=cm_cr_arp_d_product_top?ie=UTF8
    #B00MNV8E0C/ref=cm_cr_arp_d_product_top?ie=UTF8
    a = URL.split('dp/')[-1]
    #B00MNV8E0C
    SKU=a.split('/')[0]
    return SKU



class AmazonReviewItem(scrapy.Item):
    ProfileName = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

    ReviewTitle = scrapy.Field(
        input_processor=MapCompose(remove_tags,strip_html5_whitespace),
        output_processor=TakeFirst()
    )

    ReviewDate = scrapy.Field(
        input_processor=MapCompose(remove_tags, get_date),
        output_processor=TakeFirst()
    )

    ReviewText = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

    ReviewStar = scrapy.Field(
        input_processor=MapCompose(remove_tags, get_review),
        output_processor=TakeFirst()
    )
    ProductCode = scrapy.Field(
        input_processor=MapCompose(get_SKU),
        output_processor=TakeFirst()
    )
    PorductTitle = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )

    ProductURL = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst()
    )



