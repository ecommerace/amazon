import scrapy
from scrapy.loader import ItemLoader
from ..items import AmazonReviewItem


class ReviewsSpider(scrapy.Spider):
    name = 'reviews'
    allowed_domains = ['amazon.com']
    start_urls = ['https://www.amazon.com/AmazonBasics-Performance-Alkaline-Batteries-Count/product-reviews/B00MNV8E0C/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews']

    def parse(self, response):
        PorductTitle = response.xpath('//a[@data-hook="product-link"]')
        ProductURL=response.xpath('//a[@data-hook="product-link"]/@href').get()
        review_list = response.xpath('//div[@data-hook="review"]')
        for review in review_list:
            loader = ItemLoader(item=AmazonReviewItem(), selector=review, response=response)
            loader.add_xpath("ProfileName", ".//span[@class='a-profile-name']")
            loader.add_xpath("ReviewTitle", ".//a[@data-hook='review-title']")
            loader.add_xpath("ReviewDate", ".//span[@data-hook='review-date']")
            loader.add_xpath("ReviewText", ".//span[@data-hook='review-body']/span")
            loader.add_xpath("ReviewStar", ".//span[@class='a-icon-alt']")
            loader.add_xpath("PorductTitle", "//a[@data-hook='product-link']")
            loader.add_value("ProductURL", response.urljoin(ProductURL))
            loader.add_value("ProductCode", ProductURL)
            yield loader.load_item()

        next_page = response.xpath("//li[@class='a-last']/a/@href").get()
        if next_page:
            yield scrapy.Request(
                url=response.urljoin(next_page),
                callback=self.parse
            )



